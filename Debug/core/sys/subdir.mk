################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../core/sys/arg.c \
../core/sys/autostart.c \
../core/sys/energest.c \
../core/sys/etimer.c \
../core/sys/process.c \
../core/sys/procinit.c \
../core/sys/rtimer.c \
../core/sys/timer.c 

OBJS += \
./core/sys/arg.o \
./core/sys/autostart.o \
./core/sys/energest.o \
./core/sys/etimer.o \
./core/sys/process.o \
./core/sys/procinit.o \
./core/sys/rtimer.o \
./core/sys/timer.o 

C_DEPS += \
./core/sys/arg.d \
./core/sys/autostart.d \
./core/sys/energest.d \
./core/sys/etimer.d \
./core/sys/process.d \
./core/sys/procinit.d \
./core/sys/rtimer.d \
./core/sys/timer.d 


# Each subdirectory must supply rules for building sources it contributes
core/sys/%.o: ../core/sys/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


